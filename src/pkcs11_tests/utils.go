package pkcs11_tests

import (
	"fmt"
	"github.com/miekg/pkcs11"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func ReadConfig() Config {
	var config Config
	viper.SetConfigName("configCryptoserver")
	viper.SetConfigType("json")
	viper.AddConfigPath("/home/kyrychok/Desktop/pkcs11-testsuite")
	viper.AddConfigPath("C:\\Users\\Kyrychok-PC\\Desktop\\pkcs11-test-suite")
	viper.AddConfigPath(os.Getenv("pkcs11-test-config"))
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
	err = viper.Unmarshal(&config)
	if err != nil {
		panic(err)
	}
	return config
}

func GetRwUserSessionHandler(t *testing.T) (uint, *pkcs11.Ctx, pkcs11.SessionHandle) {
	config := ReadConfig()

	ctx := InitializeLibrary()

	slots, err := ctx.GetSlotList(true)
	assert.Nil(t, err, err)

	if len(slots) == 0 {
		panic("Cannot find initialized slot")
	}

	if config.CreateToken {
		prepareToken(err, ctx, slots, config, t.Name(), t)
	}

	session, err := ctx.OpenSession(slots[0], pkcs11.CKF_SERIAL_SESSION|pkcs11.CKF_RW_SESSION)
	err = ctx.Login(session, pkcs11.CKU_USER, config.UserPassword)
	assert.Nil(t, err, err)

	return slots[0], ctx, session
}

func prepareToken(err error, p *pkcs11.Ctx, slots []uint, config Config, testname string, t *testing.T) {
	err = p.InitToken(slots[0], config.SoPassword, testname)
	AssertNil(err)
	session, err := p.OpenSession(slots[0], pkcs11.CKF_SERIAL_SESSION|pkcs11.CKF_RW_SESSION)
	AssertNil(err)
	err = p.Login(session, pkcs11.CKU_SO, config.SoPassword)
	AssertNil(err)
	err = p.InitPIN(session, config.UserPassword)
	AssertNil(err)
	err = p.Logout(session)
	AssertNil(err)
}

func InitializeLibrary() *pkcs11.Ctx {
	config := ReadConfig()
	if _, err := os.Stat(config.PathToLibrary); os.IsNotExist(err) {
		panic("file with library does not exist")
	}
	p := pkcs11.New(config.PathToLibrary)
	if p == nil {
		panic("failed to load library")
	}
	err := p.Initialize()
	if err != nil {
		panic(err)
	}

	return p
}

func SearchShortcut(ctx *pkcs11.Ctx, session pkcs11.SessionHandle, attributes []*pkcs11.Attribute, occurences int) []pkcs11.ObjectHandle {
	err := ctx.FindObjectsInit(session, attributes)
	AssertNil(err)
	objects, _, err := ctx.FindObjects(session, occurences)
	AssertNil(err)
	err = ctx.FindObjectsFinal(session)
	AssertNil(err)
	return objects
}

func DestroyBothKey(ctx *pkcs11.Ctx, session pkcs11.SessionHandle, publicKeyHandler pkcs11.ObjectHandle, privateKeyHandler pkcs11.ObjectHandle) {
	err := ctx.DestroyObject(session, publicKeyHandler)
	AssertNil(err)
	err = ctx.DestroyObject(session, privateKeyHandler)
	AssertNil(err)
}

func RsaKeyToAsn1Format(ctx *pkcs11.Ctx, session pkcs11.SessionHandle, privateKeyHandler pkcs11.ObjectHandle) {
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_PRIVATE_EXPONENT, nil),
		pkcs11.NewAttribute(pkcs11.CKA_PUBLIC_EXPONENT, nil),
		pkcs11.NewAttribute(pkcs11.CKA_EXPONENT_1, nil),
		pkcs11.NewAttribute(pkcs11.CKA_EXPONENT_2, nil),
		pkcs11.NewAttribute(pkcs11.CKA_PRIME_1, nil),
		pkcs11.NewAttribute(pkcs11.CKA_PRIME_2, nil),
		pkcs11.NewAttribute(pkcs11.CKA_COEFFICIENT, nil),
		pkcs11.NewAttribute(pkcs11.CKA_MODULUS, nil)})
	AssertNil(err)
	fmt.Printf("PrivateKey in ASN.1 format:\n" +
		"asn1=SEQUENCE:private_key\n" +
		"[private_key]\n" +
		"version=INTEGER:0\n\n")
	for _, attribute := range attributes {
		attributeName := "?"
		if attribute.Type == pkcs11.CKA_PRIVATE_EXPONENT {
			attributeName = "d"
		}
		if attribute.Type == pkcs11.CKA_MODULUS {
			attributeName = "n"
		}
		if attribute.Type == pkcs11.CKA_PRIME_1 {
			attributeName = "p"
		}
		if attribute.Type == pkcs11.CKA_PRIME_2 {
			attributeName = "q"
		}
		if attribute.Type == pkcs11.CKA_COEFFICIENT {
			attributeName = "coeff"
		}
		if attribute.Type == pkcs11.CKA_EXPONENT_2 {
			attributeName = "exp1"
		}
		if attribute.Type == pkcs11.CKA_EXPONENT_1 {
			attributeName = "exp2"
		}
		if attribute.Type == pkcs11.CKA_PUBLIC_EXPONENT {
			attributeName = "e"
		}
		fmt.Printf("%v=INTEGER:%X\n\n", attributeName, attribute.Value)
	}
}

func AssertNil(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func AssertNotNil(err error) {
	if err == nil {
		panic(err.Error())
	}
}

func EndTest(ctx *pkcs11.Ctx, session pkcs11.SessionHandle) {
	err := ctx.CloseSession(session)
	AssertNil(err)
	err = ctx.Finalize()
	AssertNil(err)
	ctx.Destroy()
}

func GenerateSymmetricKey(ctx *pkcs11.Ctx, session pkcs11.SessionHandle, sensitive bool) pkcs11.ObjectHandle {
	config := ReadConfig()
	wrapKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_AES),
		pkcs11.NewAttribute(pkcs11.CKA_VALUE_LEN, config.SecretKeyLengthBits/8),
		pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, sensitive),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
		pkcs11.NewAttribute(pkcs11.CKA_DERIVE, true),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_UNWRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP_WITH_TRUSTED, false),
	}
	wrapKeyHandle, err := ctx.GenerateKey(session,
		[]*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_KEY_GEN, nil)}, wrapKeyTemplate)
	AssertNil(err)
	return wrapKeyHandle
}

func UploadKnownSymmetricKeyWithAllPermissions(ctx *pkcs11.Ctx, session pkcs11.SessionHandle) pkcs11.ObjectHandle {
	config := ReadConfig()
	wrapKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, config.WrappingKeyType),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_UNWRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_ENCRYPT, true),
		pkcs11.NewAttribute(pkcs11.CKA_DECRYPT, true),
		pkcs11.NewAttribute(pkcs11.CKA_VALUE, "0011223344556677"),
	}
	secretKeyHandler, err := ctx.CreateObject(session, wrapKeyTemplate)
	AssertNil(err)
	attrs, err := ctx.GetAttributeValue(session, secretKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_ENCRYPT, nil), pkcs11.NewAttribute(pkcs11.CKA_DECRYPT, nil)})
	AssertNil(err)
	for _, attr := range attrs {
		if attr.Type == pkcs11.CKA_ENCRYPT {
			fmt.Printf("Uploaded key has CKA_ENCRYPT set to %v\n", attr.Value)
		}
		if attr.Type == pkcs11.CKA_DECRYPT {
			fmt.Printf("Uploaded key has CKA_DECRYPT set to %v\n", attr.Value)
		}
	}
	return secretKeyHandler
}
