package tests

import (
	"fmt"
	"github.com/miekg/pkcs11"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
)

func Test_WrapRsaKeyWithAes(t *testing.T) {
	config := pkcs11_tests.ReadConfig()
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	_, privateKey := prepareRsaKey(ctx, session)
	wrapSecretKeyHandler := prepareWrapSecretKey(ctx, session)
	print(privateKey, wrapSecretKeyHandler)
	wrappedKey, err := ctx.WrapKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(config.WrappingMechanism, nil)}, wrapSecretKeyHandler, privateKey)
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Wrapped key - %x\n", wrappedKey)
	attr, err := ctx.GetAttributeValue(session, wrapSecretKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE, nil)})
	pkcs11_tests.AssertNil(err)
	fmt.Printf("It was wrapped using %v mechanism with secret key %x\n", config.WrappingMechanism, attr[0].Value)
	pkcs11_tests.EndTest(ctx, session)
}

func prepareWrapSecretKey(ctx *pkcs11.Ctx, session pkcs11.SessionHandle) pkcs11.ObjectHandle {
	config := pkcs11_tests.ReadConfig()
	wrapKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_AES),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, false),
		pkcs11.NewAttribute(pkcs11.CKA_UNWRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
		pkcs11.NewAttribute(pkcs11.CKA_VALUE_LEN, config.SecretKeyLengthBits/8),
	}

	secretKeyHandler, err := ctx.GenerateKey(session,
		[]*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_KEY_GEN, nil)},
		wrapKeyTemplate)
	pkcs11_tests.AssertNil(err)
	return secretKeyHandler
}

func prepareRsaKey(ctx *pkcs11.Ctx, session pkcs11.SessionHandle) (pkcs11.ObjectHandle, pkcs11.ObjectHandle) {
	config := pkcs11_tests.ReadConfig()
	publicKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_PUBLIC_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_RSA),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_ENCRYPT, true),
		pkcs11.NewAttribute(pkcs11.CKA_MODULUS_BITS, config.KeyLengthBits),
	}
	privateKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_PRIVATE_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_RSA),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_PRIVATE, true),
		pkcs11.NewAttribute(pkcs11.CKA_SIGN, true),
		pkcs11.NewAttribute(pkcs11.CKA_UNWRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_DECRYPT, true),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
		pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false),
	}
	publicKeyHandle, privateKeyHandle, err := ctx.GenerateKeyPair(session,
		[]*pkcs11.Mechanism{pkcs11.NewMechanism(config.TargetKeyGenerationMechanism, nil)},
		publicKeyTemplate, privateKeyTemplate)
	pkcs11_tests.AssertNil(err)
	pkcs11_tests.RsaKeyToAsn1Format(ctx, session, privateKeyHandle)
	err = ctx.SetAttributeValue(session, privateKeyHandle, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, true)})
	pkcs11_tests.AssertNil(err)

	return publicKeyHandle, privateKeyHandle
}

func Test_WrapRsaWithAes(t *testing.T) {
	config := pkcs11_tests.ReadConfig()
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	secretKeyHandler := pkcs11_tests.UploadKnownSymmetricKeyWithAllPermissions(ctx, session)
	_, privateKey := prepareRsaKey(ctx, session)
	wrappedKey, err := ctx.WrapKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(config.WrappingMechanism, nil)}, secretKeyHandler, privateKey)
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Wrapped key - %x\n", wrappedKey)

	err = ctx.DecryptInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(config.WrappingMechanism, nil)}, secretKeyHandler)
	pkcs11_tests.AssertNil(err)
	decryptedKey, err := ctx.Decrypt(session, wrappedKey)
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Decrypted key (should contain all values from ASN.1 formmated key written above) - %x\n", decryptedKey)
	pkcs11_tests.EndTest(ctx, session)
}
