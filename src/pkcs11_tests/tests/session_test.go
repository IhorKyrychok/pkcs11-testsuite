package tests

import (
	"fmt"
	"github.com/miekg/pkcs11"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
	"time"
)

func Test_SessionInterception(t *testing.T) {
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	fmt.Printf("Got handler with value %v", session)
	go func(handler pkcs11.SessionHandle) {
		time.Sleep(time.Millisecond * 300)
		ctx := pkcs11_tests.InitializeLibrary()
		obj, err := ctx.CreateObject(session, []*pkcs11.Attribute{
			pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
			pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_GENERIC_SECRET),
			pkcs11.NewAttribute(pkcs11.CKA_TOKEN, false),
			pkcs11.NewAttribute(pkcs11.CKA_VALUE, "0011223344556677"),
			pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false),
		})
		pkcs11_tests.AssertNil(err)
		fmt.Printf("%v", obj)
	}(session)
	time.Sleep(time.Millisecond * 1000)
	pkcs11_tests.EndTest(ctx, session)
}
