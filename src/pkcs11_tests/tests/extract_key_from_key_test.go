package tests

import (
	"fmt"
	"github.com/miekg/pkcs11"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
)

func Test_ExtractKeyFromKey(t *testing.T) {
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	key := pkcs11_tests.GenerateSymmetricKey(ctx, session, false)
	attr, err := ctx.GetAttributeValue(session, key, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE, nil)})
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Generated secret key with value %x or %b\n", attr[0].Value, attr[0].Value)
	newKey, err := ctx.DeriveKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_EXTRACT_KEY_FROM_KEY, []byte{0b00000011, 0, 0, 0})}, key, []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_VALUE_LEN, 128),
		//pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false),
	})
	pkcs11_tests.AssertNil(err)
	newAttr, err := ctx.GetAttributeValue(session, newKey, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE_LEN, nil)})
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Generated derived key with length %x\n", newAttr[0].Value)
	pkcs11_tests.AssertNil(err)
	pkcs11_tests.EndTest(ctx, session)
}
