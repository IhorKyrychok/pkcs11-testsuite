package tests

import (
	"encoding/binary"
	"fmt"
	"github.com/miekg/pkcs11"
	"pkcs11-test-suite/src/pkcs11_tests"
	"strings"
	"testing"
)

func Test_PaddingOracle(t *testing.T) {
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	keyValue := fmt.Sprintf("%x", 0x1111000000000000)
	fmt.Printf("Using Key 0x%v\n", keyValue)
	attribute := prepareKeyAttributes(keyValue)
	knownKeyHandler, err := ctx.CreateObject(session, attribute)
	pkcs11_tests.AssertNil(err)

	unknownKey := fmt.Sprintf("%x", 0x1111567812345678)
	fmt.Printf("Uploading (unknown) key 0x%v\n", unknownKey)
	secondKeyAttributes := prepareKeyAttributes(unknownKey)
	unknownKeyHandler, err := ctx.CreateObject(session, secondKeyAttributes)
	pkcs11_tests.AssertNil(err)

	unknownKeyAttr, err := ctx.GetAttributeValue(session, unknownKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE_LEN, nil)})
	pkcs11_tests.AssertNil(err)
	unknownKeyLength := binary.LittleEndian.Uint32(unknownKeyAttr[0].Value)

	IV := 0xFFFFFFFF
	fmt.Printf("Using IV %x\n", IV)

	gcmParams := pkcs11.NewGCMParams([]byte(fmt.Sprintf("%v", IV)), []byte(fmt.Sprintf("%v", IV)), 128)
	wrappedKey, err := ctx.WrapKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_GCM, gcmParams)}, knownKeyHandler, unknownKeyHandler)
	fmt.Printf("Wrapped unknown key - 0x%x\n", wrappedKey)

	emptyMessage := strings.Repeat("0", int(unknownKeyLength))
	encryptedMessage := initAndEncrypt(IV, ctx, session, knownKeyHandler, emptyMessage)

	fmt.Printf("Unknown key - 0x")
	for i := uint32(0); i < unknownKeyLength; i++ {
		fmt.Printf("%x", encryptedMessage[i]^wrappedKey[i])
	}
	fmt.Printf("\n")
	pkcs11_tests.EndTest(ctx, session)
}

func prepareKeyAttributes(keyValue string) []*pkcs11.Attribute {
	attribute := []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_AES),
		pkcs11.NewAttribute(pkcs11.CKA_VALUE, keyValue),
		pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, true),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
		pkcs11.NewAttribute(pkcs11.CKA_DERIVE, true),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_UNWRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP_WITH_TRUSTED, false)}
	return attribute
}

func initAndEncrypt(IV int, ctx *pkcs11.Ctx, session pkcs11.SessionHandle, key pkcs11.ObjectHandle, message string) []byte {
	gcmParams := pkcs11.NewGCMParams([]byte(fmt.Sprintf("%v", IV)), []byte(fmt.Sprintf("%v", IV)), 32)
	err := ctx.EncryptInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_GCM, gcmParams)}, key)
	pkcs11_tests.AssertNil(err)
	encryptedMessage, err := ctx.Encrypt(session, []byte(message))
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Encrypted message (0x%v) - 0x%x\n", message, encryptedMessage)
	return encryptedMessage
}
