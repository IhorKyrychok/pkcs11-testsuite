package tests

import (
	"fmt"
	"github.com/miekg/pkcs11"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
)

func Test_KeyBinding_Des3_from_0_to_16(t *testing.T) {
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)

	key := generateDesKey(ctx, session, pkcs11.NewMechanism(pkcs11.CKM_DES3_KEY_GEN, nil), pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_DES3))
	secretKeyHandler := prepareAesKey(ctx, session)

	wrappedKey, err := ctx.WrapKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_ECB, nil)}, secretKeyHandler, key)
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Wrapped key value %x, it takes %v bytes in memory\n", wrappedKey, len(wrappedKey))

	halfSizeWrappedKey := wrappedKey[0:16]
	importHalfSizeKey(halfSizeWrappedKey, ctx, session, secretKeyHandler, pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_DES2))

	secondHalfSizeWrappedKey := wrappedKey[16:32]
	importHalfSizeKey(secondHalfSizeWrappedKey, ctx, session, secretKeyHandler, pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_DES2))

	pkcs11_tests.EndTest(ctx, session)
}

func Test_KeyBinding_Des2_from_0_to_8_with_zeroing(t *testing.T) {
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)

	key := generateDesKey(ctx, session, pkcs11.NewMechanism(pkcs11.CKM_DES2_KEY_GEN, nil), pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_DES2))
	secretKeyHandler := prepareAesKey(ctx, session)

	wrappedKey, err := ctx.WrapKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_ECB, nil)}, secretKeyHandler, key)
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Wrapped key value %x, it takes %v bytes in memory\n", wrappedKey, len(wrappedKey))

	halfSizeWrappedKey := wrappedKey[0:16]
	for i := 8; i < 16; i++ {
		halfSizeWrappedKey[i] = 0
	}
	importHalfSizeKey(halfSizeWrappedKey, ctx, session, secretKeyHandler, pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_DES))

	pkcs11_tests.EndTest(ctx, session)
}

func importHalfSizeKey(halfSizeWrappedKey []byte, ctx *pkcs11.Ctx, session pkcs11.SessionHandle, secretKeyHandler pkcs11.ObjectHandle, keyType *pkcs11.Attribute) {
	switch keyType.Value[0] {
	case pkcs11.CKK_DES2:
		fmt.Printf("Trying to unwrap %x as 2DES key, it has length of %v bytes\n", halfSizeWrappedKey, len(halfSizeWrappedKey))
	case pkcs11.CKK_DES:
		fmt.Printf("Trying to unwrap %x as DES key, it has length of %v bytes\n", halfSizeWrappedKey, len(halfSizeWrappedKey))
	default:
		fmt.Printf("Unknown type")
	}
	newKeyHandler, err := ctx.UnwrapKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_ECB, nil)}, secretKeyHandler,
		halfSizeWrappedKey, []*pkcs11.Attribute{
			pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
			keyType,
			pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
			pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false),
			pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
		})
	pkcs11_tests.AssertNil(err)
	newKeyValue, err := ctx.GetAttributeValue(session, newKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE, nil)})
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Imported new DES2 key %x\n", newKeyValue[0].Value)
}

func prepareAesKey(ctx *pkcs11.Ctx, session pkcs11.SessionHandle) pkcs11.ObjectHandle {
	secretKeyHandler := pkcs11_tests.GenerateSymmetricKey(ctx, session, false)
	secretKeyAttr, err := ctx.GetAttributeValue(session, secretKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE, nil)})
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Using wrapping key with value: %x\n", secretKeyAttr[0].Value)
	return secretKeyHandler
}

func generateDesKey(ctx *pkcs11.Ctx, session pkcs11.SessionHandle, mechanism *pkcs11.Mechanism, keyType *pkcs11.Attribute) pkcs11.ObjectHandle {
	keyAttributes := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
		keyType,
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
	}
	key, err := ctx.GenerateKey(session, []*pkcs11.Mechanism{mechanism}, keyAttributes)
	pkcs11_tests.AssertNil(err)
	attr, err := ctx.GetAttributeValue(session, key, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE, nil)})
	pkcs11_tests.AssertNil(err)
	switch mechanism.Mechanism {
	case pkcs11.CKM_DES3_KEY_GEN:
		fmt.Printf("Generated DES3 with value %x, or %x %x %x\n", attr[0].Value, attr[0].Value[0:8], attr[0].Value[8:16], attr[0].Value[16:24])
	case pkcs11.CKM_DES2_KEY_GEN:
		fmt.Printf("Generated DES2 with value %x, or %x %x\n", attr[0].Value, attr[0].Value[0:8], attr[0].Value[8:16])
	default:
		fmt.Printf("Unknown mechanism")
	}
	return key
}
