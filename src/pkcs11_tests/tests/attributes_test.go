package tests

import (
	"github.com/miekg/pkcs11"
	"github.com/stretchr/testify/assert"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
)

func Test_ModifyExtractable_AllowedChange(t *testing.T) {
	ctx, err, session, publicKeyHandler, privateKeyHandler := preparePrivateKey(t, true, true)
	pkcs11_tests.AssertNil(err)
	err = ctx.SetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, false)})
	pkcs11_tests.AssertNil(err)
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, nil)})
	pkcs11_tests.AssertNil(err)
	assert.Equal(t, attributes[0].Type, uint(pkcs11.CKA_EXTRACTABLE))
	assert.Equal(t, attributes[0].Value, []byte{0})
	pkcs11_tests.DestroyBothKey(ctx, session, publicKeyHandler, privateKeyHandler)
	pkcs11_tests.EndTest(ctx, session)
}

func Test_ModifyExtractable_ProhibitedChange(t *testing.T) {
	ctx, err, session, publicKeyHandler, privateKeyHandler := preparePrivateKey(t, false, true)
	pkcs11_tests.AssertNil(err)
	err = ctx.SetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true)})
	pkcs11_tests.AssertNotNil(err)
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, nil)})
	pkcs11_tests.AssertNil(err)
	assert.Equal(t, attributes[0].Type, uint(pkcs11.CKA_EXTRACTABLE))
	assert.Equal(t, attributes[0].Value, []byte{0})
	pkcs11_tests.DestroyBothKey(ctx, session, publicKeyHandler, privateKeyHandler)
	pkcs11_tests.EndTest(ctx, session)
}

func Test_ModifySensitive_ProhibitedChange(t *testing.T) {
	ctx, err, session, publicKeyHandler, privateKeyHandler := preparePrivateKey(t, false, true)
	pkcs11_tests.AssertNil(err)
	err = ctx.SetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false)})
	pkcs11_tests.AssertNotNil(err)
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, nil)})
	pkcs11_tests.AssertNil(err)
	assert.Equal(t, attributes[0].Type, uint(pkcs11.CKA_SENSITIVE))
	assert.Equal(t, attributes[0].Value, []byte{1})
	pkcs11_tests.DestroyBothKey(ctx, session, publicKeyHandler, privateKeyHandler)
	pkcs11_tests.EndTest(ctx, session)
}

func Test_ModifySensitive_AllowedChange(t *testing.T) {
	ctx, err, session, publicKeyHandler, privateKeyHandler := preparePrivateKey(t, false, false)
	pkcs11_tests.AssertNil(err)
	err = ctx.SetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, true)})
	pkcs11_tests.AssertNil(err)
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, nil)})
	pkcs11_tests.AssertNil(err)
	assert.Equal(t, attributes[0].Type, uint(pkcs11.CKA_SENSITIVE))
	assert.Equal(t, attributes[0].Value, []byte{1})
	pkcs11_tests.DestroyBothKey(ctx, session, publicKeyHandler, privateKeyHandler)
	pkcs11_tests.EndTest(ctx, session)
}

func preparePrivateKey(t *testing.T, extractable bool, sensitive bool) (*pkcs11.Ctx, error, pkcs11.SessionHandle, pkcs11.ObjectHandle, pkcs11.ObjectHandle) {
	config := pkcs11_tests.ReadConfig()
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	publicKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_PUBLIC_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_RSA),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_ENCRYPT, true),
		pkcs11.NewAttribute(pkcs11.CKA_MODULUS_BITS, config.KeyLengthBits),
	}
	privateKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_PRIVATE_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_RSA),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_PRIVATE, true),
		pkcs11.NewAttribute(pkcs11.CKA_SIGN, true),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, extractable),
		pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, sensitive),
	}
	publicKeyHandler, privateKeyHandler, err := ctx.GenerateKeyPair(session,
		[]*pkcs11.Mechanism{pkcs11.NewMechanism(config.TargetKeyGenerationMechanism, nil)},
		publicKeyTemplate, privateKeyTemplate)
	pkcs11_tests.AssertNil(err)
	return ctx, err, session, publicKeyHandler, privateKeyHandler
}

func Test_CkaNeverExtractable_True(t *testing.T) {
	ctx, err, session, publicKeyHandler, privateKeyHandler := preparePrivateKey(t, false, true)
	pkcs11_tests.AssertNil(err)
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_NEVER_EXTRACTABLE, nil)})
	pkcs11_tests.AssertNil(err)
	assert.Equal(t, attributes[0].Type, uint(pkcs11.CKA_NEVER_EXTRACTABLE))
	assert.Equal(t, attributes[0].Value, []byte{1})
	pkcs11_tests.DestroyBothKey(ctx, session, publicKeyHandler, privateKeyHandler)
	pkcs11_tests.EndTest(ctx, session)
}

func Test_CkaNeverExtractable_False(t *testing.T) {
	ctx, err, session, publicKeyHandler, privateKeyHandler := preparePrivateKey(t, true, true)
	pkcs11_tests.AssertNil(err)
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_NEVER_EXTRACTABLE, nil)})
	pkcs11_tests.AssertNil(err)
	assert.Equal(t, attributes[0].Type, uint(pkcs11.CKA_NEVER_EXTRACTABLE))
	assert.Equal(t, attributes[0].Value, []byte{0})
	pkcs11_tests.DestroyBothKey(ctx, session, publicKeyHandler, privateKeyHandler)
	pkcs11_tests.EndTest(ctx, session)
}

func Test_LabelAttributeChange(t *testing.T) {
	ctx, err, session, publicKeyHandler, privateKeyHandler := preparePrivateKey(t, false, true)
	pkcs11_tests.AssertNil(err)
	newLabel := "newLabel"
	err = ctx.SetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_LABEL, newLabel)})
	pkcs11_tests.AssertNil(err)
	attributes, err := ctx.GetAttributeValue(session, privateKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_LABEL, nil)})
	pkcs11_tests.AssertNil(err)
	assert.Equal(t, attributes[0].Type, uint(pkcs11.CKA_LABEL))
	assert.Equal(t, attributes[0].Value, []byte(newLabel))
	pkcs11_tests.DestroyBothKey(ctx, session, publicKeyHandler, privateKeyHandler)
	pkcs11_tests.EndTest(ctx, session)
}
