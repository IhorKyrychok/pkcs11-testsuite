package tests

import (
	"github.com/miekg/pkcs11"
	"github.com/stretchr/testify/assert"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
)

func Test_Cleanup(t *testing.T) {
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	RemoveSensitiveObjectsWith(t, ctx, session, []byte{1})
	RemoveSensitiveObjectsWith(t, ctx, session, []byte{0})
	pkcs11_tests.EndTest(ctx, session)
}

func RemoveSensitiveObjectsWith(t *testing.T, ctx *pkcs11.Ctx, session pkcs11.SessionHandle, attributeValue interface{}) {
	objects := pkcs11_tests.SearchShortcut(ctx, session, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_TOKEN, attributeValue)}, 999)
	for _, objectHandle := range objects {
		err := ctx.DestroyObject(session, objectHandle)
		assert.Nil(t, err, err)
	}
}
