package tests

import (
	"fmt"
	"github.com/miekg/pkcs11"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
)

func Test_SecretKeyGenerationAndRead(t *testing.T) {
	config := pkcs11_tests.ReadConfig()
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	wrapKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_GENERIC_SECRET),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, false),
		pkcs11.NewAttribute(pkcs11.CKA_UNWRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_SENSITIVE, false),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
		pkcs11.NewAttribute(pkcs11.CKA_VALUE_LEN, config.SecretKeyLengthBits/8),
	}
	secretKeyHandler, err := ctx.GenerateKey(session,
		[]*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_GENERIC_SECRET_KEY_GEN, nil)},
		wrapKeyTemplate)
	pkcs11_tests.AssertNil(err)
	attr, err := ctx.GetAttributeValue(session, secretKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_VALUE, nil)})
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Generated secret key - %x\n", attr[0].Value)
	pkcs11_tests.EndTest(ctx, session)
}

func Test_SecretKeyGeneration_EncryptDecryptRoles(t *testing.T) {
	config := pkcs11_tests.ReadConfig()
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	wrapKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, config.WrappingKeyType),
		pkcs11.NewAttribute(pkcs11.CKA_WRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_DECRYPT, true),
		pkcs11.NewAttribute(pkcs11.CKA_ENCRYPT, true),
		pkcs11.NewAttribute(pkcs11.CKA_UNWRAP, true),
		pkcs11.NewAttribute(pkcs11.CKA_EXTRACTABLE, true),
	}
	wrapKeyHandle, err := ctx.GenerateKey(session,
		[]*pkcs11.Mechanism{pkcs11.NewMechanism(config.WrappingKeyGenerationMechanism, nil)},
		wrapKeyTemplate)
	pkcs11_tests.AssertNil(err)
	wrappingKeyAttributes, err := ctx.GetAttributeValue(session, wrapKeyHandle, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_DECRYPT, nil)})
	pkcs11_tests.AssertNil(err)

	if wrappingKeyAttributes[0].Value[0] == 0 {
		fmt.Printf("Wrapping key doesn't have CKA_DECRYPT attribute -> cannot decrypt wrapping key")
		t.SkipNow()
	}
	pkcs11_tests.EndTest(ctx, session)
}

func Test_SecretKeyWriting(t *testing.T) {
	config := pkcs11_tests.ReadConfig()
	_, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)
	secretKeyHandler := pkcs11_tests.UploadKnownSymmetricKeyWithAllPermissions(ctx, session)
	err := ctx.EncryptInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(config.WrappingMechanism, nil)}, secretKeyHandler)
	pkcs11_tests.AssertNil(err)
	encrypted, err := ctx.Encrypt(session, []byte("abcd"))
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Encrypted text - %v\n", encrypted)
	pkcs11_tests.EndTest(ctx, session)
}
