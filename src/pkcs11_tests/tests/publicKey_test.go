package tests

import (
	"encoding/binary"
	"fmt"
	"github.com/miekg/pkcs11"
	"pkcs11-test-suite/src/pkcs11_tests"
	"testing"
)

func Test_createPublicKey(t *testing.T) {
	config := pkcs11_tests.ReadConfig()
	slotId, ctx, session := pkcs11_tests.GetRwUserSessionHandler(t)

	tokenInfo, err := ctx.GetTokenInfo(slotId)
	pkcs11_tests.AssertNil(err)

	fmt.Printf("Total private memory:%v\nTotal public memory:%v\n", tokenInfo.TotalPrivateMemory, tokenInfo.TotalPublicMemory)
	fmt.Printf("Free private memory:%v\nFree public memory:%v\n", tokenInfo.FreePrivateMemory, tokenInfo.FreePublicMemory)

	publicKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_PUBLIC_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_RSA),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
		pkcs11.NewAttribute(pkcs11.CKA_MODULUS_BITS, config.KeyLengthBits),
	}
	privateKeyTemplate := []*pkcs11.Attribute{
		pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_PRIVATE_KEY),
		pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_RSA),
		pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
	}
	publicKeyHandler, _, err := ctx.GenerateKeyPair(session,
		[]*pkcs11.Mechanism{pkcs11.NewMechanism(config.TargetKeyGenerationMechanism, nil)},
		publicKeyTemplate, privateKeyTemplate)

	attr0, err := ctx.GetAttributeValue(session, publicKeyHandler, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_CLASS, nil)})
	pkcs11_tests.AssertNil(err)

	if binary.LittleEndian.Uint32(attr0[0].Value) == uint32(pkcs11.CKO_PUBLIC_KEY) {
		fmt.Printf("Generated public RSA key has attribute CKO_PUBLIC_KEY\n")
	} else {
		fmt.Printf("Generated public RSA key does not have attribute CKO_PUBLIC_KEY\n")
	}
	tokenInfo, err = ctx.GetTokenInfo(slotId)
	pkcs11_tests.AssertNil(err)
	fmt.Printf("Free private memory:%v\n Free public memory:%v\n", tokenInfo.FreePrivateMemory, tokenInfo.FreePublicMemory)
	pkcs11_tests.EndTest(ctx, session)
}
