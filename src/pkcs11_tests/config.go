package pkcs11_tests

type Config struct {
	Name                           string `json:"Name"`
	PathToLibrary                  string `json:"PathToLibrary"`
	SoPassword                     string `json:"SoPassword"`
	UserPassword                   string `json:"UserPassword"`
	CreateToken                    bool   `json:"CreateToken"`
	WrappingKeyType                uint   `json:"WrappingKeyType"`
	WrappingKeyGenerationMechanism uint   `json:"WrappingKeyGenerationMechanism"`
	TargetKeyGenerationMechanism   uint   `json:"TargetKeyGenerationMechanism"`
	WrappingMechanism              uint   `json:"WrappingMechanism"`
	DecryptMechanism               uint   `json:"DecryptMechanism"`
	KeyLengthBits                  int    `json:"KeyLengthBits"`
	SecretKeyLengthBits            int    `json:"SecretKeyLengthBits"`
	PrivateKeyPath                 string `json:"PrivateKeyPath"`
}
