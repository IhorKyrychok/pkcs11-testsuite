module pkcs11-test-suite

go 1.13

require (
	github.com/miekg/pkcs11 v1.0.3
	github.com/spf13/viper v1.6.3
	github.com/stretchr/testify v1.3.0
)
